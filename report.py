# -*- coding: utf-8 -*-
from trytond.report import Report

__all__ = ['RegistroGuardias']

class RegistroGuardias(Report):
    __name__ = 'report.guardias'

    @classmethod
    def get_context(cls, records, data):
      report_context = super(RegistroGuardias, cls).get_context(records, data)
      return report_context
